import React from 'react';

function VehiculeCard({ vehicule }) {
    return (
        <div className="Bien">
            <p>{vehicule.brand}</p>
            <p>{vehicule.model}</p>
        </div>
    );
}

export default VehiculeCard;