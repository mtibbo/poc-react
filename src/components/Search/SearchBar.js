import React from 'react';


function SearchBar({ onSearch }) {


    return (

        <form className="form-inline mb-2">
            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
            <button className="btn btn-outline-success my-2 my-sm-0" type="submit" onClick={onSearch}>Search</button>
        </form>

    );
}

export default SearchBar;
