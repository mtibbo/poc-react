import React, { useState } from 'react';
import SubscriberList from '../Subscriber/SubscriberList';
import SearchBar from './SearchBar';


function SearchContainer() {
    const [subscribers, subscriberHasBeenSearch] = useState([]);

    const handleOnSearch = (e) => {
        e.preventDefault();
        subscriberHasBeenSearch([
            {
                firstName: "Test",
                lastName: "Test"
            },
            {
                firstName: "Test1",
                lastName: "Test1"
            },
            {
                firstName: "Test2",
                lastName: "Test2"
            },
            {
                firstName: "Test3",
                lastName: "Test3"
            },
            {
                firstName: "Test4",
                lastName: "Test4"
            }
        ])
    }

    return (
        <div className="mt-5">
            <SearchBar onSearch={handleOnSearch} />
            {subscribers.length > 0 && <SubscriberList subscribers={subscribers} />}
        </div>
    );
}

export default SearchContainer;
