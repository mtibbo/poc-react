import React from 'react';


function SubscriberItem({ subscriber }) {
    return (
        <div className="col-md-3 mb-2">
            <div className="card">
                <div className="card-body">
                    <p>{subscriber.lastName}</p>
                    <p>{subscriber.firstName}</p>
                </div>
            </div>
        </div>
    );
}

export default SubscriberItem;
