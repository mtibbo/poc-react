import React from 'react';
import SubscriberItem from './SubscriberItem';


function SubscriberList({ subscribers }) {
    return (
        <div className="row">
             {Object.keys(subscribers).map(key => <SubscriberItem key={key} subscriber={subscribers[key]} />)}
        </div>
    );
}

export default SubscriberList;
