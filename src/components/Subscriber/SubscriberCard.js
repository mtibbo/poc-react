import React from 'react';

function SubscriberCard({ subscriber }) {
    return (
        <div className="Souscripteur">
            <p>{subscriber.firstname}</p>
            <p>{subscriber.lastname}</p>
        </div>
    );
}

export default SubscriberCard;