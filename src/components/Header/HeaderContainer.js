import React from 'react';
import HeaderNav from './HeaderNav';


function HeaderContainer({ onLogout }) {
    return <HeaderNav onLogout={onLogout} />
}

export default HeaderContainer;
