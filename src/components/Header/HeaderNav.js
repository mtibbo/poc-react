import React from 'react';
import { Link } from "react-router-dom";


function HeaderNav({ onLogout }) {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">Higgins</a>
            <div className=" navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link" to="/qualification">Qualification</Link>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" onClick={onLogout}>Logout</a>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default HeaderNav;
