import React, { useState } from 'react';
import { SearchPostalAddress } from 'search-postal-address';
import { Carto } from 'react-demo-carto';


class Qualification extends React.Component {

    state = {
        value: {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.290084,
                    49.897443
                ]
            },
        }
    };

    handleSelectedAddress = addr => {
        console.log(addr);
        this.setState(addr)
    };

    render() {
        return (
            <div className="Qualification">

                <Carto centerLatitude={this.state.value.geometry.coordinates[1]} centerLongitude={this.state.value.geometry.coordinates[0]} />
                <SearchPostalAddress onAddressSelected={this.handleSelectedAddress} />
            </div>
        );
    }
}

export default Qualification;