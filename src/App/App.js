import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import QualificationPage from "../pages/Qualification"
import HomePage from "../pages/Home"
import Login from "../pages/Login"
import HeaderContainer from '../components/Header/HeaderContainer';

function App() {
  const [isAuthenticated, userHasAuthenticated] = useState(false);
  const handleOnSubmit = (e) => {
    e.preventDefault();
    userHasAuthenticated(true);
  }

  const handleOnLogout = () => {
    userHasAuthenticated(false);
  }

  return !isAuthenticated ? (<Login onSubmit={handleOnSubmit} />) : (
    <Router>
      <HeaderContainer onLogout={handleOnLogout} />

      {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/qualification">
          <QualificationPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
